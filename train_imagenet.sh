#!/bin/bash

DATA_TRAIN=$ARNOLD_TRAIN
DATA_VAL=$ARNOLD_TEST
OUTPUT=$ARNOLD_OUTPUT

# train_imagenet.py exists in lab.gpu.mxnet image.
exec python /root/mxnet-rdma/example/image-classification/train_imagenet.py --data-train $DATA_TRAIN --data-val $DATA_VAL $@